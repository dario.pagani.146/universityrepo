# Analisi 1

## 26 - 09 - 2018

### Informazioni Generali

* Prof. Luigi Berselli
* Email: luigi.carlo.berselli@unipi.it
  * Scrivere Oggetto: Analisi 2018
* Sito internet http://pagine.dm.unipi.it/berselli
* Libri consigliati:
  * Pagani-Salsa, Analisi Matematica 1, Zanichelli
  * Salsa-Squellati, Esercizi di Analisi Matematica 1, Zanichelli
* Per gli esami: http://esame.unipi.it

------

### Funzioni di variabili reali

* Funzione: 
  * Necessita di 3 oggetti
    1. Dominio
    2. Codominio / Immagine
    3. Relazione

#### Insieme

Può diventare ambiguo, per esempio avendo un insieme definito tramite una relazione si può avere la descrizione "Le lettere dell'alfabeto" perché esistono molti alfabeti.
$$
A = \{ Lettere\ Alfabeto\ Italiano \} \\
'a' \in A \\
'k' \notin A \\
B = \{ x : x>3 \} \\\
C = \{ x : x^2<0 \} = \empty
$$
Diagramma: serve per visualizzare gli elementi.

* Eulero Venn: tipo di diagramma

$$
A = \{ a,b,c \} \\
B = \{ a,b,c,d \} \\
A \subseteq B
$$

Tra insiemi si possono fare:

* Unione
* Intersezione

$$
C = \{ 1,3,5 \} \\
D = \{ 1,2,4,5 \} \\
C \cap D = \{ 1,5 \} \\
C \cup D = \{ 1,2,3,4,5 \} \\
E = \{ a,b,c,d \} \\
F = \{ a,b \} \\
F \subseteq E (sottoinsieme) \\
$$

#### Relazione

Ad ogni elemento del dominio associa uno ed uno solo elemento del codominio.

Es:
$$
A = \{ a,b,c \} \\
B = \{ 1,2,3,4 \} \\
f(a) = 1 \\
f(b) = 2 \\
f(c) = 1
$$
Mantenendo gli stessi insiemi, una funzione non definita può essere:
$$
f(a) = 1 \\
f(b) = 5 \quad errore\\
f(c) = 1
$$
Un'altra relazione errata si verifica quando un elemento del dominio ha due o più valori nel codominio.

#### Immagine / Codominio

$$
I(f) = \{ b \in B : \exist a : f(a) = b \}
$$

#### Funzione Iniettiva

$$
(f,A,B) \quad iniettiva \\
\forall a_1,a_2,a_n \in A \quad a_n \ne a_{n+1} \quad f(a_n) \ne f(a_{n+1})
$$

#### Funzione Surgettiva

$$
(f,A,B) \quad surgettiva \\
\forall b \in B \quad \exist a \in A : f(a) = b
$$

La funzione parabolica/quadratica con dominio reale è surgettiva.
$$
f(x) = x^2 \\
D = I = \R \\
f(1) = f(-1) = 1
$$

#### Prodotto Cartesiano

$$
A x B = \{ (a,b) \quad a \in A, b \in B \} \\
A = \{ \alpha, \beta, \gamma \} \\
B = \{ 1,2 \} \\
A x B = \{ (\alpha,1),(\alpha,2), (\beta,1), (\beta,2), (\gamma,1), (\gamma,2) \}
$$

Graficamente (diagramma cartesiano) pensa al punto.

#### Cardinalità

$$
A \subseteq B \quad A  \ne B \\
\#A \le \#B \\
ex. \\
A = \N \\
B = \{ n \in \N : 2m \quad m \in \N \} \\
A = \{1,2,3,4,...\} \\
B = \{2,4,6,8,...\} \\
f(n) = 2n \\
n \ne m \\
f(n) \ne f(m) \\
2n \ne 2m \\
n \ne m \rightarrow True \\
f \ A \rightarrow B
$$

f(n) è una funzione iniettiva e surgettiva (bigettiva)

#### Grafico

È l'insieme dei punti ottenuti dalla funzione

### Esercizi

Dimostrare che:
$$
f(x) = x^2 \\
D = I = \R^+ \\
x \ne y \rightarrow f(x) = x^2 \ne y^2 = f(y) \quad \forall x,y \geq 0 \\
x^2 - y^2 = 0 \rightarrow (x-y)(x+y) = 0
$$
è iniettiva e/o bigettiva.

La funzione è iniettiva perché entrambi i numeri sono non-negativi ed il loro prodotto non è mai 0

### Insiemi

#### Naturali

$$
\N = \{1, 2, 3, ...\} \\
\N x \N \ni (m,n) \rightarrow m+n = n+m \in \N
$$

Contare significa associare (mettere in relazione) degli elementi all'insieme dei numeri naturali.

Cardinalità:
$$
\#A = 5
$$
Esempio dell'albergo con stanze infinite, tra insiemi infiniti si può verificare la corrispondenza biunivoca anche tra insiemi diversi, con cardinalità diverse, non succede invece tra insiemi finiti.

Appunto: Il professore è stato chiaro fin'ora, da una parte non vedo l'ora di fare gli integrali. Personalmente nei numeri naturali considero anche 0 per un motivo filosofico: contare significa individuare l'unità, e quindi anche l'esistenza dell'Io, non credo sia possibile individuare l'esistenza se non hai idea della non-esistenza, un po' in linea col pensiero binario Kantiano (luce ed ombra, maschio e femmina, etc.); di conseguenza per me lo 0 è nei naturali perché per contare devi conoscere l'unità e se conosci l'unità conosci anche l'opposto. Mi devo ricordare di metterlo in un breve appunto prima dei calcoli/dimostrazioni per evitare ambiguità, sopratutto quando passeremo all'argomento della matematica discreta.

## 27 - 09 - 2018

#### Funzione bigettiva

Si dice funzione bigettiva (uno ad uno, ogni elemento del Dominio ha esattamente una relazione con un elemento dell'Immagine) quella funzione che è sia Iniettiva, sia Surgettiva.

Esiste ed è uno solo si indica con l'aggiunta del punto escalamativo, la definizione formale è:
$$
\forall b \in B \quad \exist! a \in A \quad : f(a) = b
$$

#### Funzione inversa

Avendo una funzione Bigettiva è possibile trovare la funzione inversa definita con:
$$
g: B \rightarrow A \\
g(f(a)) = a \quad \forall a \in A \quad g(b)=a \\
f(g(b)) = b \forall b \in B \\
f^{-1} = g \ne \frac{1}{f}
$$

#### Funzione crescente

Es con la quadratica
$$
x > y \ge 0 \\
x^2 > y^2 \rightarrow x^2-y^2 > 0 \rightarrow (x-y)(x+y) > 0
$$
Definizione (f è strettamente crescente se):
$$
f:D \rightarrow \R \\
\forall x,y \in D : x<y \rightarrow f(x) < f(y)
$$
Definizione (f è crescente se):
$$
f:D \rightarrow \R \\
\forall x,y \in D : x \le y \rightarrow f(x) \le f(y)
$$

#### Seno, Coseno e funzioni inverse

La funzione inversa del seno(o coseno) esiste solo nel dominio:
$$
x \in \left[- \frac{1}{2}\pi, \frac{1}{2}\pi \right]
$$
Valore assoluto sin(x):
$$
|sin(x)| =  \begin{cases}
sin(x) \quad if \; sin(x) \ge 0  \\
-sin(x) \quad if \; sin(x) < 0
\end{cases}
$$
Quindi più genericamente il valore assoluto di una funzione è:
$$
|f(x)| = \{ f(x) \quad if \; f(x) \ge 0 \\
\{ f(x) \quad if \; f(x) < 0
$$

Devo sistemare l'indentazione o trovare il modo di mostrare queste funzioni "a tratti", magari mi stampo un handbook di LaTeX.

#### Radicali

$$
\sqrt{x^2} = | x | \\
|x| = \{ x \quad if \; x \ge 0 \\
\ \ \ \ \ \ \ \ \ \ \{ -x \quad if \; x \le 0
$$

#### Valore Assoluto

Proprietà:
$$
|x| \ge 0 \quad \forall x \in \R \\
|x| = 0 <=> x = 0 \\
-|x| \le x \le |x| \quad \forall x \in \R \\
|xy| = |x|\times|y|
$$
Es. disequazione
$$
|x| \le r \quad r>0 \\
x \le r \quad 0 \le x \\
-x \le r \quad x \le 0 \\
-r \le x \le r
$$

#### Disuguaglianza triangolare

$$
|x+y| \le |x| + |y| \quad \forall x,y \in \R
$$

Dimostrazione:
$$
-|x| \le x \le |x| \\
-|y| \le y \le |y| \\
(somma) \\
-(|x|+|y|) \le x+y \le |x|+|y| \\
(25) \\
|x+y| \le r \le |x|+|y|
$$

#### Insiemi numerici

##### Numeri Naturali

$$
\N
$$

L'operazione principale, sempre ammessa, dei naturali è la somma

##### Numeri Interi

$$
\Z \\
\N \subseteq \Z
$$

Moltiplicazione, operazione definita nei numeri interi
$$
\Z \times \Z \rightarrow \Z \\
n \times m = n*m = n+n+n (m \, volte)
$$
La moltiplicazione ha la proprietà commutativa e associativa (ereditate dalla somma) e distributiva

##### Numeri frazionari

$$
\Q = \left\{ \frac{n}{m} : n \in \Z, m \in \N \right\} \\
\frac{n}{m} = \frac{k}{e} \iff ne = km
$$

Questi tre insiemi di numeri sono chiamati razionali
$$
\N \subseteq \Z \subseteq \Q
$$
Reciproco
$$
x^{-1} = \frac{1}{x} \implies x^{-1} \times x = 1
$$
Opposto
$$
x+(-x) = 0
$$
Esercizio

Scrivere f e g, dove g è la funzione inversa
$$
f:\N \rightarrow \Z \\
1 \rightarrow 0 \\ 
2 \rightarrow 1 \\
3 \rightarrow -1 \\
4 \rightarrow 2 \\
5 \rightarrow -2 \\
...
$$
Sono tutti insiemi infiniti, è anche possibile metterli in relazione bigettiva
$$
\# \N = \# \Z = \# \Q
$$

### Divisione intera

Divisione: con divisione si parla di divisione "intera", trovando quoziente e resto
$$
a / b \quad a=bq+r \quad 0 \le r < b
$$
Esempio:
$$
21 / 4 \\
21 = 5*4+1
$$
Per calcolare la radice si può usare il metodo delle bisezioni, ovvero:
$$
x = \sqrt 2 \\
1 < x < 2 \rightarrow 1 < x^2 < 4
$$
Si calcola il quadrato dei numeri decimali e si confrontano, riducendo di metà ogni volta il range (1, 2 - 1, 1.5 - 1.25, 1.5 - etc).
$$
\frac{1}{3} = 0.33333 ... = \frac{3}{10} + \frac{3}{100} + ... \\
\frac{1}{5} = 0.20000 ... = \frac{2}{10} + \frac{0}{100} + ... \\
\frac{1}{3} + \frac{1}{5} = \frac{5+3}{15}
$$

## 28 - 09 - 2018

### Continuo della divisione intera


$$
a,b \in \N \quad a > b > 0 \\
q,r \in \N \quad 0 \le r < b \\
r \in \{ 0,1,2,3,...,b-1 \} \\
a = bq+r \\
$$
q è il quoziente, r è il resto, b è il divisore

### Divisione non intera

$$
0 < a <b \\
a / b = ? \\
$$

Il professore ha mostrato la divisione classica, non la riporto, esempio con 1/3

### Somme tra numeri periodici

$$
\frac{1}{3} = 0.33333... \\
x = 0.77777777... = \frac{7}{10} + \frac{7}{100} + ...  \\
10x = 7.777... \\
9x = 10x - x = 7 \\
x = \frac{7}{9} \\
\frac{1}{3} + \frac{7}{9} = 1
$$

### Dimostrazione di anti-periodo e periodo

L'anti-periodo sono tutte le cifre precedenti il periodo, il periodo sono le cifre che si ripetono.
$$
m,n \in \N \\
a = anti-periodo \\
b = periodo \\
x = 0.a_0,a_1,...,a_n,b_0,b_1,...,b_n \\
10^{n+m}x-10^mx = x(10^{m+n}-10^m)
$$
es.
$$
x = 0.1\overline{23} = 0.123232323 \\
10x = 1.23232323 \\
10^2 10x = 123.2323 \\
\mathfrak{Guarda\ che\ fighezza}
$$


#### Separare numeri periodici ai numeri frazionari normali

In breve basta considerare che i numeri frazionari finiti sono tutti divisori di una potenza di 10 quindi 2 e/o 5.

### Radice di 2

$$
\sqrt{2} = 1.4142... \\
\sqrt{2} =? \frac{p}{q} (ridotta\ ai\ minimi\ termini) \\
p,q \in \N \\
\frac{p}{q} = \sqrt 2 <=> \frac{p^2}{q^2} = 2 <=> p^2 = 2q^2 \\
p^2\ è\ pari \\
((2m)^2 = 4m^2 )\ pari\\
((2m - 1)^2 = 4m^2 - 4m + 1 )\ dispari\\
$$

Quindi il quadrato di un numero pari è sempre pari, mentre il quadrato di un numero dispari è sempre dispari.
$$
Continuo(46)\\
p=2m \\
(2m)^2 = 2q^2 \iff 4m^2 = 2q^2 \iff 2m^2 = q^2
$$
Non è possibile ridurre la radice di 2 in una frazione poiché sia p che q sono pari, pertanto è possibile ridurre/semplificare ulteriormente la frazione.

### Massimo

Il massimo di un insieme è definito da:
$$
M \in \R \quad M = max(A) \\
M \ge x \quad \forall x \in A \\
M \in A
$$

$$
A = \{1,2,3,20\} \quad max(A) = 20 \\
\# A < + \infin
$$

Ovviamente l'insieme dev'essere finito.

### Periodici al limite

Sorge un problema con i numeri periodici del tipo:
$$
u.\overline{9} \approx u + 1
$$

### Intervalli

Un intervallo può essere chiuso o aperto.

Chiuso:
$$
[a,b] = \{ x\in\R : a\le x \le b \}
$$
Per indicare un intervallo aperto si può usare la parentesi tonda () o la parentesi quadra al contrario. 

es.
$$
] a,b [ = \{ x\in\R : a < x < b \}
$$
Nel caso di intervalli aperti non è presente il massimo (o il minimo), per indicare il numero massimo o minimo non compreso si parla di limite superiore o inferiore.

Di massimo ce n'è uno ed uno solo, infatti dimostriamolo per assurdo:
$$
M_1 \ge x \quad \forall x \in A\\
M_1 \in A \\
M_2 \ge x \quad \forall x \in A\\
M_2 \in A \\
---\\
M_1 < M_2 \\
M_1 \ge x \quad \forall x \in A \\
x = M_2 \\
M_1 \ge M_2 \\
M_2 \ge M_1 \\
M_1 = M_2
$$

Il minimo di un insieme è definito da:
$$
m \in \R \quad m = min(A) \\
m \le x \quad \forall x \in A \ne \emptyset \\
m \in A
$$
Esempio:
$$
A = \{1,2,3,20\} \quad m = min(A) = 1 \\
$$

### MCD

#### Algoritmo di Euclide

$$
a \ge b \quad a,b \in \N \\
a = bq + r \quad q,r \in \N \\
$$

Bisogna trovare il più grande divisore comune tra a e b, indicato con u
$$
a = su \quad s \in \N \\
b = tu \quad t \in \N \\
(48) \\
su = tuq + r (r = u(s-tq)) \\
v | b \quad v|r \\
b = vb \quad r = vz \\
a = vpq + vz = v (pq+z) \\
$$
Ricapitolando tutto divide tutto quindi è possibile trovare un divisore comune tra a, b ed r
$$
A = \{ u \in \N : u|a,u|b \} \\
B = \{ u \in \N : u|b, ur \} \\
A = B \\
A \le \R
$$
Usando la definizione di massimo possiamo definire il MCD come:
$$
a>b \\
MCD (a,b) = (a,b) \\
A = \{ u \in \N : u|a \and u|b \} \ne \emptyset \\
B = \{ u \in \N : u|b \and u|r \} \ne \emptyset \\
MCD(a,b) = max(A) \\
$$
Es.
$$
a = 1804 \quad b = 328 \\
a = 328 q + r = 1804 =328*5+164 \\
MCD(164,0) = MCD(328, 164) = MCD(1804,328) \\
a = 328 \quad b = 164 \\
328 = 164 q + r = 328 = 164 * 2 + 0 \\
MCD = 164
$$

Compito: Fare un programma che esegue l'MCD.

Appunto: Ricordare di chiedere del libro.

## 01 - 10 - 2018

## 02 - 10 - 2018

### Ricorsione

Definizione di funzione ricorsiva:
$$
f(n) = \begin{cases}
a_n = f(a_{n-1}) \\
a_0 = k
\end{cases}
$$
Alcuni esempi di funzioni ricorsive o ricorrenti sono:

1. Numeri di Fibonacci
2. Numero fattoriale

Esempio della funzione fattoriale:
$$
f(n) = \begin{cases}
	a_0 = 1 \\
	a_{n+1} = a_n (n+1)
\end{cases}
$$

Scrivere il fascio di rette che passa per un punto assegnato, esempio:
$$
P = (x_0, y_0) \\
f(x) - f(x_0) = m(x - x_0) \quad Scrittura\ di\ traslazione \\
f(x) = m(x-x_0) + f(x_0)
$$
Scrivere la funzione di una retta dati due punti:
$$
x_0 \ne x_1 \\
P_0 = (x_0, y_0) \\
P_1 = (x_1, y_1) \\
y - y_0 = m(x - x_0) \\
y_1 - y_0 = m(x_1 = x_0) \\
m = \frac{y_1 - y_0}{x_1 - x_0}
y - y_0 = \frac{y_1 - y_0}{x_1 - x_0} (x-x_1)
$$

### Insiemi - limiti

$$
0 \ne A \subseteq \R
$$

Se A è limitato superiormente si verifica questa condizione:
$$
\exist k \in \R : x \le k \forall x \in A
$$
Esempio:
$$
\{ x \in \Q : 0 \le x < \sqrt 2 \} = A \ne \emptyset \\
3 \ge x \quad \forall x \in  A
$$
k viene quindi definito "Maggiorante di/per", per condizione quindi gli insiemi con massimo hanno un maggiorante.
$$
A = \bigcup_{i=0}^\infin \left[2i, 2i+\frac{1}{2} \right]
$$
A non è limitato superiormente se non ha maggioranti.

Definizione:
$$
\forall k \in \R \quad \exist x \in A : x > k
$$
Per dimostrarlo bisogna solvere la disequazione:
$$
k < x_k
$$
Come esiste il limite superiore esiste il limite inferiore. k di conseguenza viene definito "minorante". nell'esempio di A 2 è un limite inferiore.

Definizione:
$$
\forall k \in \R \quad \exist x \in A : x < k
$$

### Insieme limitato

Un insieme si dice limitato se è limitato sia inferiormente che superiormente.

Un esempio
$$
A = \{ x \ge 3 \} \implies \nexists max(A)  \\
B = \{ x : x < 1 \and x \ge 0 \} = [0,1)
$$

### Estremi

Lambda è un estremo superiore. Si tratta anzitutto di un maggiorante ed è il minimo tra i maggioranti.
$$
\empty \ne A \subseteq \R \\
\Lambda = sup(A)
$$
Definizione di estremo superiore:
$$
\Lambda \ge x \forall x \in A \\
\forall \epsilon > 0 \quad \exist\ \overline x \in a : \quad \overline x > \Lambda - \epsilon
$$
Esempio:
$$
1 = sup[0,1)
$$


Dimostriamo che il massimo è anche il limite superiore.
$$
Prop. if \ M = max(A) \implies M = sup(A) \\
1. \quad M \ge x \quad \forall x \in A \\
2. \quad M \in A \implies \forall \epsilon > 0 \exist \overline x \in A : \overline x > M - \epsilon
$$
Esempio:
$$
1.\quad sup(A) =  M \ge x \quad \forall x \in A \\
1 = sup [0,1) \\
2. \quad M \in A \implies \forall \epsilon > 0 \exist \overline x \in A : \overline x > M - \epsilon
$$
Teorema

Ip: Se A è limitato superiormente ...

Th: Esiste un numero Lambda tale che Lambda è l'estremo superiore, quindi tutti gli insiemi limitati superiormente hanno un estremo superiore.
$$
\exist \Lambda \in \R : \Lambda = sup(A)
$$
Dm:

1. Ipotesi:

$$
x \in A \\
\exist k \in \R \quad 0 \le x \le k \quad \forall x \in A \\
0 \le [x] \le k \\
---- \\
A = [0,1) \\
[x]_0 = .9 \\
[x]_1 = .99\\
[x]_2 = .999 \\
\lim_{n \rightarrow \infin}[x]_n = sup(A) = 1 \\
$$

Così è possibile trovare l'estremo superiore (attenzione: è uno solo perché è IL minimo dei maggioranti).
$$
A = \left\{ x = 1 - \frac{1}{n} : n \in \N \right\} = \left\{ 0, \frac{1}{2}, \frac{2}{3}, ... \right\} \\
\forall \epsilon > 0 \quad \exist \overline x \in A : 1 - \frac{1}{\overline n} > 1- \epsilon \implies \frac{1}{\overline n} < \epsilon \implies \overline n > \frac{1}{\epsilon} \\
$$

## 03 - 10 - 2018

### Estremo superiore

Tornando alla definizione di estremo superiore si ha:
$$
\Lambda = sup(f) = sup(Im(f)) \\
\Lambda \ge f(x) \quad \forall x \in D \\
\forall \epsilon > 0 \quad \exist\ \overline x \in D : f(\overline x) > \Lambda - \epsilon \\
M = max(f) \implies M = sup(f)
$$
Esempio:
$$
D = \{ x>0 : x \in \R \} \\
f(x) = 1 - \frac{1}{x} \\
f(x) = (-\infin ,1) \\
Dimostrazione\ estremo\ superiore: \\
1 - \frac{1}{\overline x} > 1 - \epsilon \iff - \frac{1}{\overline x} > - \epsilon \iff \frac{1}{\overline x} < \epsilon \iff \overline x > \frac{1}{\epsilon}
$$
Insieme limitato superiormente:
$$
\exist K : \quad f(x) \le K \quad \forall x \in D \\
$$

Se invece si parla di insieme non limitato superiormente basta negare la proposizione 83.

Altra funzione:
$$
f(x) = \begin{cases}
1 \quad x \notin \N \\
n \quad x = n \in \N
\end{cases} \\
f: \R \rightarrow \R \\
sup(f(x)) = + \infin
$$
Esercizio:

* Disegna il grafico della funzione

$$
f(x) = sin \left( \frac{1}{x} \right) \\ 
CE \quad x \ne 0 \\
D = \R
$$

Altri esempi:
$$
\emptyset \ne A = \left\{ x \in \Q^+ : x^2 \le 2 \right\} \subseteq \Q \\
$$
Questo insieme ha come estremo superiore radice di 2, anche se non è un numero incluso in Q, non è un numero razionale.

Quindi quando si parla di estremi si parla dell'insieme dei numeri Reali in ogni caso.

### Minimo

$$
m \le x \quad \forall x \in A \\
m \in A \\
\lambda = inf(A) \\ 
se \\
1.\quad \lambda \le x \quad \forall x \in A \\
2.\quad \forall \epsilon > 0 \quad \exist\ \overline x \in A : \overline x < \lambda + \epsilon
$$

### Insieme limitato inferiormente

$$
\emptyset \ne A \subseteq \R \ limitato \ inferiormente \implies \\
\implies \exist\ \lambda \in \R \quad \lambda = inf(A)
$$

Se esiste un minimo dell'insieme A ovviamente questo corrisponde al limite inferiore.

E quindi:
$$
1.\quad \lambda \le f(x) \quad \forall x \in D \\
2.\quad \forall \epsilon > 0 \quad \exist\ \overline x \in D : f(\overline x) < \lambda + \epsilon
$$

### Successioni

$$
f:\N \rightarrow \R \\
\exist\ k, K: \quad k \le f \le K \quad \forall n \in \N
$$

Esempio 1:
$$
f(x) = (-1)^x \quad x \in \N \\
limitata\ perche': \\
-1 \le f(x) \le 1
$$
Esempio 2:
$$
f(x) = x^2
$$
Osservazioni:

* Tutti i numeri al quadrato sono positivi, inoltre la funzione è solo nei numeri naturali. Pertanto il limite inferiore è 1 (se 0 non è considerato tra i numeri Naturali)
* Ora bisogna dimostrare se è limitata superiormente

$$
\forall M \in \R \quad \exists \overline x : \quad f(\overline x) = \overline x^2 > M \\
--- \\
Dimostrazione \\
n^2 \ge n \iff n^2-n \ge 0 \iff n(n-1) \ge 0
$$

Il grafico di una successione è non-continuo a causa del dominio dei numeri naturali.

### Successione di Erone e def. limite

Formula di Erone:
$$
f(x) = \begin{cases}
x_0 = \alpha > \sqrt 2 \\
x_{n+1} = \frac{1}{2} \left( x_n + \frac{2}{x_n} \right)
\end{cases}
$$
Limite: x "si avvicina"/"tende" a radice di 2:
$$
\lim_{n \rightarrow + \infin} x_n = \sqrt 2
$$
Più in generale:
$$
\lim_{x \rightarrow \pm \infin} f(x) = L \in \R 
$$
Il limite di f(x) è L per n che tende ad infinito, x converge a L per x che tende a infinito.

Definizione limite:
$$
\forall \epsilon > 0 \quad \exist\ \overline n \in \N: \quad |x_n - L | < \epsilon \quad \forall n > \overline n
$$
Esempio con iperbolica:
$$
0 < x_n = \frac{1}{n} \\
\lim_{n \rightarrow + \infin} 1/n = 0 = L \\
\forall \epsilon > 0 \quad \exists \overline n \in \N: \quad -\epsilon < \frac{1}{n} < \epsilon \\
\epsilon > 0 \quad \frac{1}{n} < \epsilon \quad n > \frac{1}{\epsilon}
$$
Esempio con un'altra funzione:
$$
f(n) = (-1)^n \\
\lim_{n \rightarrow +\infin} f(x) \ne 1 \ne -1 \implies L = \nexists
$$
In questo caso non esiste limite perché la funzione "non tende" né ad 1 né a -1, varia in base alla natura di n, se è pari o dispari.

Esercizio: Dimostrare che il limite non esiste.

### Esiste un solo limite?

Se esiste un solo limite si verifica la seguente uguaglianza:
$$
\lim_{x \rightarrow +\infin} f(x) = L = \lim_{n \rightarrow +\infin} f(n) = M \implies \\
\implies \\ 
\forall \epsilon > 0 \quad \exist\ \overline n \in \N: \quad |x_n - L | < \epsilon \quad \forall n > \overline n \\
\forall \epsilon > 0 \quad \exist\ \overline{\overline n} \in \N: \quad |x_n - M | < \epsilon \quad \forall n > \overline{\overline n}
$$
Suponiamo che L ed M siano diversi:
$$
L \ne M \implies L > M \\
\epsilon = \frac{L-M}{3}
$$
La dimostrazione passa per il fatto che è impossibile che nelle fasce di L ed M esistano due valori f(x) per la stessa x.

### Potenza n-esima di un binomio

$$
(a+b)^n = \sum_{k=0}^n \binom n k a^{n-k} b^k \\
\binom n k = \frac{n!}{k!(n-k)!}
$$
