#include <stdio.h>

int euclid_gcd(int a, int b)
{
	while (a != b)
	{
		if (a > b)
		{
			a = a - b;
		}
		else
		{
			b = b - a;
		}
	}
	return a;
}

int main(int argc, char const *argv[])
{
	int a = 1804;
	int b = 328;

	printf("The Greatest Common Divisor between %i and %i is %i \n", a, b, euclid_gcd(a, b));

	return 0;
}
