'''
Euclidean algorithm implementation
This script is used to find the GCD (Greatest Common Divisor) 
between two numbers using the Euclid's Algorithm
'''

def euclid_gcd(a=1, b=1):
	'''
	euclid_gcd(a,b) -> int
	Desc: Return the Greatest Common Divisor between two numbers using the Euclid Algorithm
	Parameters:
	a -- The first number (integer type)
	b -- The second number (integer type)
	'''
	if type(a) is int and type(b) is int:
		while a != b:
			if a > b:
				a = a - b
			else:
				b = b - a
		return a
	else:
		raise TypeError("Both numbers must be integers: " +
		                str(type(a)) + " " + str(type(b)) + " were passed")

def main():
	'''
	Test the functions
	'''
	a = 1804
	b = 328
	print("The greatest common divisor between",
	      a, "and", b, "is", euclid_gcd(a, b))

	a = 0.5
	b = 10

	print("The greatest common divisor between",
	      a, "and", b, "is", euclid_gcd(a, b))

if __name__ == '__main__':
	main()
