'''
Heron formula/algorithm implementation
This script is used to find the square root of a given number
using the Heron's Formula
'''
# Import decimal library to have better precision
import decimal as dc
# Precision up to 100 decimal values
dc.getcontext().prec = 100


def heron_sqrt(s, x_n=1, n_iterations=1000):
    '''
    heron_sqrt(int, float, int) -> Decimal
    Desc:
    1. Begin with an arbitrary positive starting value x0
    (the closer to the actual square root of S, the better).
    2. Let x_{n+1} be the average of xn and S/xn
    (using the arithmetic mean).
3. Repeat step 2 until the desired accuracy is achieved.
    Parameters:
    a -- The supposed starting square of the number -> The square of the number
    s -- The number to square (integer type)
    '''

    # Check if x_n is a positive integer number
    if isinstance(x_n, int) and x_n > 0 and isinstance(n_iterations, int):
        # Convert to decimal number
        x_n = dc.Decimal(x_n)
        s = dc.Decimal(s)

        for _ in range(0, n_iterations):
            x_n = (x_n + s/x_n) / 2

        return x_n
    else:
        raise TypeError("The number to square must be positive and an integer "
                        + " and the iterations count must be integers: "
                        + str(x_n) + str(type(x_n)) + str(type(n_iterations)) + " were passed")


def main():
    '''
    Test the function
    '''

    print(heron_sqrt(250, 1))
    print(heron_sqrt(4, 1))
    print(heron_sqrt(2, 1))


if __name__ == '__main__':
    main()
