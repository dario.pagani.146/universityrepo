#include <iostream>

int main()
{
	// signed int
	int a;
	int b = 1;
	int c(3);
	int d, e, f;
	
	short int g;
	short h;
	long int i;
	
	// Octals, Hexadecimals numbers
	int oct = 056;
	int hex = 0xAB;
	
	std::cout<<oct<<" "<<hex;

	// unsigned int
	unsigned u;
	unsigned long ul;
	unsigned short us;

	// decimals
	double d; // 64 bit
	float f; // 32 bit
	long double ld; // 80 bit

	// characters, at least 8 bits
	char ch = "a";

	// const
	const float pi = 3.14;
	// Errore! pi = 3.15;
	// delete(pi);
	// const float pi = 3.15;

	// conditions
	bool r = 1 > 0; // true, 1
	r = 1 < 0; // false, 0
	r = 1 > 0 && 1 < 0; // false, 0
	r = 1 > 0 || 1 < 0; // true, 1

	return 0;
}
