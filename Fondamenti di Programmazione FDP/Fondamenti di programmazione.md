# Fondamenti di programmazione

## 27 - 09 - 2018

### Informazioni Generali

#### Docenti

* Prof. Gianluca Dini + Pericle Perazzo, Giovanni Nardini
* Email: gianluca.dini@unipi.it, pericle.perazzo@unipi.it, giovanni.nardini@unipi.it
* Sito: http://www.iet.unipi.it/g.dini/Teaching/fdp/matdid/index.html
* Oggetto: FDP 

#### Contenuti del corso

* Architettura Computer di Neuman (immagino)
* Concetti di base della programmazione (definizioni, sintassi, algoritmo, etc.)
* Programmazione C
* Programmazione a oggetti (teoria)
* Programmazione C++
* Strutture particolari (liste, code, pile)

#### Orario

* Lunedì - 13:30 - 15:30, F9
* Martedì - 15:30 - 18:30, F9
* Giovedì - 15:30 - 17:30, F9 (esercitazione, obbligo di notebook)

#### Esame

* Prova Pratica
  * Risolvere un problema tramite programmazione
* Prova Orale
  * Domande teoriche (eh?)

### Algoritmo, problema, programma

Si presenta un problema, l'algoritmo è una sequenza precisa (non ambigua) e finita di operazioni che portano soluzione al problema.

Es

Lancio di un dado

```
Creare un numero random da 1 a 6 pallepallepalle
Scrivere in output il numero ottenuto pallepallepalle
Fine
```

Es 2

Consumo medio di un auto

```scheme
Acquisire i dati necessari: (litri utilizzati, km alla partenza, km all'arrivo)
Poni il valore di "distanza percorsa" a (km arrivo - km partenza)
Poni il valore di "km al litro" (distanza percorsa / litri utilizzati)
Scrivere in output il valore di "km al litro"
Fine
```

Creare un algoritmo per risolvere un problema permette l'automatizzazione della soluzione, che però necessita di una macchina che esegua i passaggi dell'algoritmo.

Il linguaggio di programmazione è un linguaggio che può essere compreso dal computer.

Il programma è un algoritmo scritto in un linguaggio di programmazione.

Il programma ha due requisiti:

1. Corretto, ovvero deve risolvere il problema
2. Efficiente, ovvero deve risolvere il problema nella maniera più veloce

### Rappresentare Informazioni

Tutte le informazioni sono rappresentate nel computer in codice binario.

Un bit è uguale a 0 o 1, 8 bit compongono un byte.

Obsolescenza dei componenti (i prodotti hanno una decadenza data dal tempo, una lampadina può, ad esempio, perdere luminosa).

Utilizzo delle combinazioni, il massimo numero rappresentabile rispetta la seguente uguaglianza:
$$
m = 2^n-1
$$
Es.
$$
n = 2 => m = 2^2-1 = 3
$$
Tabella di conversione:

| m    | p0   | p1   |
| ---- | ---- | ---- |
| 0    | 0    | 0    |
| 1    | 0    | 1    |
| 2    | 1    | 0    |
| 3    | 1    | 1    |

Convenzionalmente 0 significa "spento", 1 significa "acceso".

La stessa codifica binaria ha comunque più usi, lo stesso numero può avere significati diversi in base al contesto (65 è 'A' ad esempio).

Provare per credere:

![IntToChar](assets/IntToChar.jpg)