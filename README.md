# What?

Questa repository contiene i miei appunti presi alle lezioni a cui sono stato presente. 

Ci sono anche alcuni libri in pdf che ho trovato e mi sono sembrati interessanti, principalmente per le materie matematiche dato che ho già abbastanza conoscenze di informatica (modestia a parte).

Negli appunti scrivo anche ogni tanto delle considerazioni e i compiti da fare per non dimenticarmeli.

Gli eventuali programmi saranno scritti (in ordine di preferenza) con i seguenti linguaggi:

1. Python
2. Rust
3. C
4. C ++